 <h1>DoD - Definiton of Done</h1>

<table>
  <thead>
    <tr>
      <th style="color: orange;">Minimum</th>
      <th style="color: yellow;">Optimum</th>
      <th style="color: green;">Maximum</th>
    </tr>
  </thead>
  <tbody>
    <tr>
    	<td>
    		<ul>
     			<li>Minimale Kriterien erfüllt</li>
     			<li>Unit Test bestanden zu 75%</li>
      			<li>Funktionelle Tests bestanden</li>
      			<li>keine bekannten Fehler</li>
      			<li>Code Review mit 2tem Entwickler</li>
      			<li>Dokumentation erstellt</li>
    		</ul>
    	</td>
        <td>
        	<ul>
        		<li>Alle Kriterien erfüllt</li>
        		<li>keine Kriterien offen</li>
        		<li>Unit Test bestanden zu 90%</li>
        		<li>Funktionelle Tests bestanden</li>
        		<li>Sauberer Code</li>
        		<li>Codekommentare vorhanden</li>
        		<li>keine bekannten Fehler</li>
        		<li>Code Review mit 2tem Entwickler</li>
        		<li>Dokumentation erstellt</li>
      	</ul>
    </td>
    <td>
    	<ul>
      		<li>Alle Kriterien erfüllt</li>
      		<li>keine Kriterien offen</li>
      		<li>Unit Test bestanden zu 100%</li>
      		<li>Funktionelle Tests bestanden</li>
      		<li>Sauberer Code</li>
      		<li>Codekommentare vorhanden</li>
      		<li>PEN Test bestanden</li>
      		<li>Load Test bestanden</li>
      		<li>Performance Test bestanden</li>
      		<li>Barrierefreiheit Zertifikat A vorhanden</li>
      		<li>keine bekannten Fehler</li>
      		<li>Code Review mit 2tem Entwickler</li>
      		<li>Dokumentation erstellt</li>
      		<li>SIKO vorhanden</li>
      		<li>Datenschutzkonzept wenn nötig vorhanden</li>
      		<li>User Acceptance Test wenn nötg vorhanden</li>
   		</ul>
    </td>
   </tr 
  </tbody>
</table>