<?php
$color1 = '#FF0200';
$color2 = '#FFA600';
$color3 = '#F4ED03';
$color4 = '#10FF00';
$color5 = '#00FFE6';
$color6 = '#001AFF';
session_start();
$points = array();
$statistik= array();

if(isset($_POST['colorchange'])){
    $color1 = $_POST['color1'];
    $color2 = $_POST['color2'];
    $color3 = $_POST['color3'];
    $color4 = $_POST['color4'];
    $color5 = $_POST['color5'];
    $color6 = $_POST['color6'];
}


if(isset($_POST["submit"])){
    $newfile = 'true';
    $file = $_FILES["file"];
    $fileName = $file["name"];
    $fileTempPath = $file["tmp_name"];
    if (($handle = fopen($fileTempPath, 'r')) !== FALSE) { // Check the resource is valid
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { // Check opening the file is OK!
        array_push($points, $data);
    }
    $_SESSION['points'] = $points;
    fclose($handle);
}else {
}
}



if(isset($_GET['x'])){
    $_SESSION['points'][$_GET['nr']][0]= $_GET['x'];
    $_SESSION['points'][$_GET['nr']][1]= $_GET['y'] ;
    $_SESSION['points'][$_GET['nr']][2]= $_GET['r'] ;
}

if($_GET['del'] == 'true'){
    unset($_SESSION['points'][$_GET['nr']]);
    $df = fopen("csv/output.csv", 'w');
   foreach ($_SESSION['points'] as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   if (($handle = fopen("csv/output.csv", 'r')) !== FALSE) { // Check the resource is valid
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { // Check opening the file is OK!
        array_push($points, $data);
    }
    $_SESSION['points'] = $points;
}
}

if(isset($_POST['selectRadius'])){
    $start = $_POST['von'];
    $end = $_POST['bis'];
    for ($i = 0; $i < count($_SESSION['points']); $i++){
        if ( $_SESSION['points'][$i][2] >= $start && $_SESSION['points'][$i][2] <= $end ){
            unset($_SESSION['points'][$i]);
        } 
    }
        $df = fopen("csv/output.csv", 'w');
   foreach ($_SESSION['points'] as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   if (($handle = fopen("csv/output.csv", 'r')) !== FALSE) { // Check the resource is valid
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) { // Check opening the file is OK!
        array_push($points, $data);
    }
    $_SESSION['points'] = $points;
        
    }
    }




if(isset($_SESSION['points'])){
$df = fopen("csv/output.csv", 'w');
   foreach ($_SESSION['points'] as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
}




for ($i = 0; $i < count($_SESSION['points']); $i++){
    array_push($statistik, $_SESSION['points'][$i][2]);
}

rsort($statistik); 
                $middle = round(count($statistik) / 2); 
                $total = $statistik[$middle-1];
                $average = array_sum($statistik) / count($statistik);

if($_GET['sessclear'] == 'true'){
    session_destroy();
}

?>



<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hensel und Gretel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
   <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">-->
    <link href="css/addons/pickr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/design.css">


</head>
<body class="bg-secondary" >

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">H&G</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <form class="form-inline ml-auto">
                <button class="btn btn-success" type="button" data-toggle="modal" data-target="#exampleModal">CSV einlesen...</button>
                <a class="btn btn-success" id="download" download="test.jpg" href="" onclick="download_img(this);">Als Grafik exportieren</a>
                <a class="btn btn-success" href="csv/output.csv">Als CSV exportieren...</a>
            </form>
        </div>
    </div>
</nav>






<div class="container-fluid">
<div class="row ">
    <div class="col-12 col-xl-2 col-lg-4 col-md-4 col-sm-4">
        <div class="modal-dialog ">
            <div class="modal-content main-section">
                <i id="logo" class="text-center fas fa-cog"></i>
                <div class="accordion" id="accordionExample">
                <div class="card">
                        <div class="card-header p-0" id="headingThree">
                            <h2 class="container  mb-0">
                                <button class="btn btn-light btn-block text-left  p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Statistik
                                </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                
                                <p>Anzahl Punkte: <?php echo count($statistik)?></p>
                                <p>Maximum Wert:   <?php echo max($statistik) ?></p>
                                <p>Minimum Wert:   <?php echo min($statistik) ?></p>
                                <p>Median:     <?php echo $total ?></p>
                                <p>Mittelwert: <?php echo $average ?></p>
                            </div>
                        
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header p-0" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-light btn-block collapsed text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Wertebereich einstellen
                                </button>
                            </h2>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <form method="post" action="index.php">
                                    <div class="row g-3">
                                    <div class="col-6">
                                        <label for="von" class="form-label">von</label>
                                        <input type="number" class="form-control" id="von" name="von" placeholder="0">
                                    </div>
                                    <div class="col-6">
                                        <label for="bis" class="form-label">bis</label>
                                        <input type="number" class="form-control" id="bis" name="bis" placeholder="100000">
                                    </div>
                                    </div>
                                   <div class="row text-center mt-2">
                                       <div class="col">
                                           <button type="submit" name="selectRadius" class="btn btn-primary mb-3">Werte ausblenden</button>
                                       </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header p-0" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-light btn-block text-left collapsed p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                   Farben einstellen
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                            <form method="post" action="index.php">
                                <div class="row mb-5">

                                        <p>Farbschema definieren</p>
                                        <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>0-20</p>
                                        <input type="color" name="color1" value="#FF0200">
                                        </div>
                                    <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>21-50</p>
                                        <input type="color" name="color2" value="#FFA600">
                                    </div>
                                    <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>51-100</p>
                                        <input type="color" name="color3" value="#F4ED03">
                                    </div>
                                </div>
                                <div class="row mb-5">

                                    <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>101-300</p>
                                        <input type="color" name="color4" value="#10FF00">
                                    </div>
                                    <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>301-600</p>
                                        <input type="color" name="color5" value="#00FFE6">
                                    </div>

                                    <div class="col d-flex justify-content-center align-items-center flex-column">
                                        <p>601-10000</p>
                                        <input type="color" name="color6" value="#001AFF">
                                    </div>

                                   

                                </div>
                               
                                <div class="row mb-5">
                                <div class="mt-2 mb-3 col d-flex justify-content-center align-items-center flex-column">
                                    <input type="submit" class="btn btn-primary" name="colorchange" value="Farben ändern">
                                    </div>
                                    <p>Hier können Sie Ihr eigenes Farbschema definieren. Hierfür klicken Sie einen Button an und ändern Sie die Farbe. Sie können Auch direkt den Farbcode angeben</p>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
        </div>
    </div>
    </div>
    <div class="col-12 col-xl-10 col-lg-8 col-md-8 col-sm-8 mt-5">
            <h1 class="text-light"> Punkte Auswertung</h1>
        <div class="container-fluid mt-5 mb-10 pb-10">
            <canvas id="bubbleChart" style="background-color: white">

            </canvas>
        </div>

    </div>

</div>





  <!-- Modal  upload-->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="post" action="index.php" enctype="multipart/form-data">
            <div class="form-file">
                <input type="file" class="form-file-input" id="file" name="file">
                <label class="form-file-label" for="customFile">
                  <span class="form-file-text">Choose file...</span>
                  <span class="form-file-button">Browse</span>
                </label>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
          <button type="submit" name="submit" class="btn btn-primary">Upload</button>
        </div>
        </form>
      </div>
    </div>
  </div>
      </div>


      <!-- Modal  change-->
      <div class="modal fade" id="change" tabindex="-1" aria-labelledby="change" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="changeTitel">Punkt anpassen</h5>
                      <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <div class="accordion" id="acc2">
                          <div class="card">
                              <div class="card-header p-0" id="headingOne">
                                  <h2 class="mb-0">
                                      <button class="btn btn-light btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#one" aria-expanded="true" aria-controls="collapseOne">
                                          Wertebereich einstellen
                                      </button>
                                  </h2>
                              </div>

                              <div id="one" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                  <div class="card-body">
                                      <form >
                                          <div class="row g-3">
                                              <div class="col-6">
                                                  <label for="x" class="form-label">x</label>
                                                  <input type="number" class="form-control" id="x" placeholder="0">
                                              </div>
                                              <div class="col-6">
                                                  <label for="y" class="form-label">y</label>
                                                  <input type="number" class="form-control" id="y" placeholder="100000">
                                              </div>
                                              <div class="col">
                                                  <label for="radius" class="form-label">Radius</label>
                                                  <input type="number" class="form-control" id="r" placeholder="0">
                                              </div>
                                          </div>
                                          <div class="row text-center mt-2">
                                              <div class="col">
                                                    <input type="hidden" id="nr" valuue="">
                                                  <button type="button" class="btn btn-primary mb-3" onclick="changeData()">Punkt verschieben</button>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                          </div>
                      <div class="card">
                          <div class="card-header p-0" id="headingThree">
                              <h2 class="mb-0">
                                  <button class="btn btn-light btn-block text-left collapsed p-3 rounded-0 " type="button" data-toggle="collapse" data-target="#two" aria-expanded="false" aria-controls="collapseThree">
                                     Punkt löschen
                                  </button>
                              </h2>
                          </div>
                          <div id="two" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                              <div class="card-body d-flex justify-content-center align-items-center flex-column">
                                  <button class="btn btn-danger" onclick="delPoint()">Punkt löschen</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                  </div>
              </div>
          </div>
      </div>




<?php  
    if(isset($_SESSION['points'])){
        for($i = 0; $i < count($_SESSION['points']); $i++){
            $counter = $i+1;
            if($_SESSION['points'][$i][2] <= 20){
                $color = $color1;
            }elseif($_SESSION['points'][$i][2] <= 50){
                $color = $color2;
            }elseif($_SESSION['points'][$i][2] <= 100){
                $color = $color3;
            }elseif($_SESSION['points'][$i][2] <= 300){
                $color = $color4;
            }elseif($_SESSION['points'][$i][2] <= 600){
                $color = $color5;
            }elseif($_SESSION['points'][$i][2] <= 10000){
                $color = $color6;
            }
            $datas .= "{
                label: 'Punkt: $counter',
                data: [{
                    x:" . $_SESSION['points'][$i][0] .",
                    y:" . $_SESSION['points'][$i][1] ." ,
                    r:" . $_SESSION['points'][$i][2] ."
                }],
                backgroundColor: '". $color ."' ,
                hoverBackgroundColor:'". $color ."'
            },";
        }
    }
    
?>







<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/js/mdb.min.js"></script>
<script src="https://kit.fontawesome.com/6e2b3102f2.js" crossorigin="anonymous"></script>
<script src="js/exportcanvas.js"></script>

<script>
var ctxBc = document.getElementById('bubbleChart').getContext('2d');
var bubbleChart = new Chart(ctxBc, {
    backgroundColor: "#ffffff",
    type: 'bubble',
    options: {
        backgroundColor: "#ffffff",
        opacity: false,
        responsive: true,
        legend: false,
    },
    data: {
        datasets: [
        <?php echo $datas; ?>
        ]
    }
    
})


$( "#bubbleChart" ).on( "click", function(evt) {
    var activePoints = bubbleChart.getElementsAtEvent(evt);
    var activePoint = bubbleChart.lastActive[0];
    if(activePoints.length > 0)
    {
        var clickedElementindex = activePoint["_datasetIndex"]
        var label = bubbleChart.data.datasets[clickedElementindex].label;
        var value = bubbleChart.data.datasets[clickedElementindex].data[0];
        console.log(activePoint["_datasetIndex"]);
        console.log(value);
        $("#change").modal()
        $("#change").find('h5.modal-title').text(label + " anpassen");
        $("#change").find('#x').val(value["x"]);
        $("#change").find('#y').val(value["y"]);
        $("#change").find('#r').val(value["r"]);
        $("#change").find('#nr').val(activePoint["_datasetIndex"]);
    }
});

function changeData(nr,x,y,r){
clickedElementindex =  document.getElementById('nr').value;
x = document.getElementById('x').value;
y = document.getElementById('y').value;
r = document.getElementById('r').value;
window.location.href = '?x='+x+'&y='+y+'&r='+r+'&nr='+clickedElementindex;


}
function delPoint(){
    clickedElementindex =  document.getElementById('nr').value;
window.location.href = '?del=true&nr='+clickedElementindex;
}


</script>
</body>
</html>